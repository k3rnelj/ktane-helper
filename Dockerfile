FROM ubuntu

RUN apt -y update
RUN apt -y install python3 make

COPY . .

ENTRYPOINT ["./build-and-print.sh"]