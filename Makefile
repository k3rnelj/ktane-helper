all: README.md

by-engagement.md: print-nicely.py categorization.tsv
	./print-nicely.py > $@

letter-pos.md: lettertab.sh
	echo "### Alphabet letter positions" > $@
	./lettertab.sh >> $@

helpful-data.md: letter-pos.md nato.md
	echo "## Helpful general data" > $@
	cat $^ >> $@

README.md: intro.md turn-the-keys.md helpful-data.md by-engagement.md
	cat $^ > $@ 

clean:
	-rm README.md
	-rm letter-pos.md by-engagement.md helpful-data.md
